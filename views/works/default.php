<html>
<head>
<link rel="stylesheet" type="text/css" href="/assets/works/default/style.css">
<script src="/assets/works/default/script.js"></script>
</head>

<body>
    <div class="work_scroll_full_100_100"><!--background color div --></div>
    <div class="work_scroll_full_100_100" id="QJGIO190" scrollbar>
        <div class="work_bk_100 hover-target" text-hover="SCROLL" style="background:black;">
            <div class="work_semi_circle">
                <div class="semi_circle" style="transform:rotate(-90deg)">
                    <svg class="size1" width="450" height="200" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <circle cx="225" cy="225" r="225" fill="white" clip-path="url(#cut-off-bottom)" />
                    </svg>
                    <svg class="size2" width="350" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <circle cx="175" cy="170" r="175" fill="white" clip-path="url(#cut-off-bottom)" />
                    </svg>
                </div>
            </div>
            <img class="bk" src="https://www.plasticbcn.com/uploads/home/hitachi-website_1441_1x.png">
        </div>
        <div class="work_width_infos_page_80">
            <div data-smooth-from="-15px" data-smooth-to="25px" class="work_page_title">Default<br>Work title.</div>
            <div class="work_main_infos_cont">
                <div class="left_cont">
                    <div class="infos_block">
                        <div class="title">Client</div>
                        <div>Default</div>
                    </div>
                    <div class="infos_block">
                        <div class="title">Location</div>
                        <div>Worldwide</div>
                    </div>
                    <div class="infos_block">
                        <div class="title">Work</div>
                        <div>Strategy, UX research,<br> concepts, messaging hierarchy, <br>web design (IA, IxD, design, UX/UI design, prototyping, animation), <br>TOV, content definition.</div>
                    </div>
                </div>
                <div class="right_cont"><div>Default is a blockless distributed ledger which overcomes the limitations of blockchain technology… I know, right? That’s why we were asked to design a concept website for potential users and investors, to help them understand the proposition and differentiate Default from its competitors in the emerging blockchain and cryptocurrency world.</div>
                <div class="link"><a class=" hover-target" href="" target="_blank">Website 1.</a></div>
                <div class="link"><a class=" hover-target" href="" target="_blank">Website 2.</a></div>
                </div>
            </div>
        </div>
        <div class="work_img_page_cont">
            <div class="cont"><div style="width:84%; margin-left:8%"><img width="100%" height="auto" src="https://codetheweb.blog/assets/img/posts/css-advanced-background-images/mountains.jpg"></div></div>
            <div class="cont">
                <div class="part_cont" style="margin-left:10%; width:30em;">
                    <div class="number">01.</div>
                    <div><div class="title">This is a title.</div>
                    <div class="description">This is a description that also means nothing but I'm writting so it takes place on my page.</div>
                    </div>
                </div>
            </div>
            <div class="cont">
                <div style="width:35%; margin-left:10%"><img width="100%" height="auto" src="https://codetheweb.blog/assets/img/posts/css-advanced-background-images/mountains.jpg"></div>
                <div data-smooth-from="5em" data-smooth-to="0" style="width:45%; margin-left:4%; margin-top:-10%;"><img width="100%" height="auto" src="https://codetheweb.blog/assets/img/posts/css-advanced-background-images/mountains.jpg"></div>            
            </div>
            <div class="cont"><div style="width:80%; margin-left:12%"><img width="100%" height="auto" src="https://codetheweb.blog/assets/img/posts/css-advanced-background-images/mountains.jpg"></div></div>
            <div data-bk="rgba(0, 0, 0)" data-color="white"></div>
            <div class="cont">
                <div style="width:25%; margin-left:8%;"><div data-pinned class="descr_cont">
                    <div class="title">This is a title.</div>
                    <div class="description">This is a description that means nothing but I'm still writing so it takes place on my page. This is a description that means nothing but I'm still writing so it takes place on my page.This is a description that means nothing but I'm still writing so it takes place on my page. This is a description that means nothing but I'm still writing so it takes place on my page.</div>
                </div></div>
                <div style="width:60%; margin-left:5%"><img width="100%" height="auto" src="https://www.plasticbcn.com/uploads/hitachi-global-website/lift/lift-planning.jpg"></div>
            </div>
            <div class="cont">
                <div style="width:25%; margin-left:65%; text-align:right;"><div class="descr_cont">
                    <div class="title">This is a title.</div>
                    <div class="description">This is a description that means nothing but I'm still writing so it takes place on my page.</div>
                </div></div>
            </div>
            <div class="cont"><div style="width:100%; margin-left:0%"><img width="100%" height="auto" src="https://codetheweb.blog/assets/img/posts/css-advanced-background-images/mountains.jpg"></div></div>
            <div class="cont">
                <div class="part_cont" style="margin-left:10%; width:30em;">
                    <div class="number">02.</div>
                    <div><div class="title">This is a title.</div>
                    <div class="description">This is a description that also means nothing but I'm writting so it takes place on my page.</div>
                    </div>
                </div>
            </div>
            <div data-bk="rgba(247, 247, 247, 1)" data-color="black"></div>
            <div class="cont"><div data-smooth-from="30px" data-smooth-to="-50px" style="width:85%; margin-left:8%"><img width="100%" height="auto" src="https://codetheweb.blog/assets/img/posts/css-advanced-background-images/mountains.jpg"></div></div>
        </div>
        <div class="work_contact_me">
            <div>Want to know more about this project?</div>
            <div id="work_contact_me_button" class="hover-target" text-hover="CONTACT"><div class="bar"></div>Contact me.</div>
        </div>
        <div class="work_bottom_page">
            <div class="pre-next-proj">Next project</div>
            <div id="next_project_trigger" class="hover-target" text-hover="NEXT" link-work="default">Default work.
            <div class="arrow_next"><i class="fas fa-arrow-right"></i></div></div>       
            <div class="txt_btm_right_GQO92">
                <div class="hover-target" text-hover="DL"><a target="_blank" href="/assets/files/Franck-Desfrancais-CV.pdf">Curriculum vitae</a></div>
                <div>·</div>    
                <div>© 2019 Franck Desfrançais.</div>
            </div>      
        </div>
    </div>
</body>