    <html>
<head>
<link rel="stylesheet" type="text/css" href="/assets/works/default/style.css">
<script src="/assets/works/default/script.js"></script>
<!--<link rel="stylesheet" type="text/css" href="/assets/works/abc/style.css">-->
</head>

<body>
    <div class="work_scroll_full_100_100"><!--background color div --></div>
    <div class="work_scroll_full_100_100" id="QJGIO190" scrollbar>
        <div class="work_bk_100 hover-target" text-hover="SCROLL" style="background:#942775;" >
            <div class="work_semi_circle">
                <div class="semi_circle" style="transform:rotate(-90deg)">
                    <svg class="size1" width="450" height="200" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <circle cx="225" cy="225" r="225" fill="#BB004A" clip-path="url(#cut-off-bottom)" />
                    </svg>
                    <svg class="size2" width="350" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <circle cx="175" cy="170" r="175" fill="#BB004A" clip-path="url(#cut-off-bottom)" />
                    </svg>
                </div>
            </div>
            <img class="bk" preload="/assets/works/abc/imgs/lowRes/logo.png" src="/assets/works/abc/imgs/logo.png">
        </div>
        <div class="work_width_infos_page_80">
            <div data-smooth-from="-15px" data-smooth-to="25px" class="work_page_title">ABC<br>Diffusion.</div>
            <div class="work_main_infos_cont">
                <div class="left_cont">
                    <div class="infos_block">
                        <div class="title">Entreprise</div>
                        <div>ABC Diffusion</div>
                    </div>
                    <div class="infos_block">
                        <div class="title">Localisation</div>
                        <div>Lyon</div>
                    </div>
                    <div class="infos_block">
                        <div class="title">Durée</div>
                        <div>6 semaines</div>
                    </div>
                    <div class="infos_block">
                        <div class="title">Travail</div>
                        <div>
                            UX recherche, concepts,<br>
                            Web design (IA, IxD, design, UX/UI design, prototyping, animation),<br>
                            Back developpeur PHP/MySQL,<br>
                            Front developpeur Html/Css/JQuery/Javascript,<br>
                            Database developpeur PhpMyAdmin,
                        </div>
                    </div>
                    <div class="infos_block">
                        <div class="title">Documentations</div>
                        <div>
                        <a class="hover-target" target="_blank" href="/assets/works/abc/files/rapport-stage-abc.doc">Rapport de stage</a><br>
                        <a class="hover-target" target="_blank" href="/assets/works/abc/files/notice-utilisation.docx">Documentation Utilisateur</a><br>
                        </div>
                    </div>
                </div>
                <div class="right_cont"><div>
                    ABC Diffusion est une sociétés spécialisée dans la ventre de meubles aux entreprises. Ma mission a été le développement
                    d'une refonte du site, en site de vente avec confirmation de commande (par email). <br> J'ai également ajouté un panel administrateur afin de faciliter l'édition
                    du site.
                    </div>
                    <div class="link"><a class=" hover-target" href="/assets/works/abc/files/competences.png" target="_blank">Voir les compétences.</a></div>
                    <div class="link"><a class=" hover-target" href="//abcdif.esy.es" target="_blank">Aller au site.</a></div>
                </div>
            </div>
        </div>
        <div class="work_img_page_cont">
            <div class="cont">
                <div class="part_cont" style="margin-left:calc(90% - 30em); width:30em; text-align:right;">
                    <div class="number">01.</div>
                    <div><div class="title">La mission.</div>
                    <div class="description"></div>
                    </div>
                </div>
            </div>
            <div class="cont">
                <div style="width:70%; margin-left:20%; text-align:right;"><div class="descr_cont">
                    <div class="title">Trouver la solution de développement.</div>
                    <div class="description">ABC souhaitant investir le moins d'argent possible et peu de compétences en informatique, le premier choix a été de chercher un CMS gratuit. L'idée des CMS a rapidement été abandonnée, les modules de ventes étant pratiquement tous payants.
                    <br>La solution retenue a donc été de développer un site de a-z avec un panel administrateur permettant la création, l'édition des produits, des catégories de produits ect.. Il fallait aussi ajouter un module d'envoi de mail (un au client, un à ABC) lors de la confirmation de commandes.
                    </div>
                </div></div>
            </div>
            <div class="cont">
                <div class="part_cont" style="margin-left:10%; width:30em;">
                    <div class="number">02.</div>
                    <div><div class="title">Le site.</div>
                    <div class="description"></div>
                    </div>
                </div>
            </div>
            <div class="cont"><div style="width:80%; margin-left:12%"><img width="100%" height="auto" preload="/assets/works/abc/imgs/lowRes/img1.png" src="/assets/works/abc/imgs/img1.png"></div></div>
            <div class="cont">
                <div style="width:35%; margin-left:10%"><img width="100%" height="auto" preload="/assets/works/abc/imgs/lowRes/img2.png" src="/assets/works/abc/imgs/img2.png"></div>
                <div data-smooth-from="5em" data-smooth-to="0" style="width:45%; margin-left:4%;"><img width="100%" height="auto" preload="/assets/works/abc/imgs/lowRes/img3.png" src="/assets/works/abc/imgs/img3.png"></div>            
            </div>
            <div class="cont">
                <div style="width:25%; margin-left:8%; text-align:right;"><div data-pinned class="descr_cont">
                    <div class="title">Confirmation d'achat.</div>
                    <div class="description">Cet email est envoyé au client et a ABC Diffusion qui se chargera de rentrer en communication avec l'acheteur.</div>
                </div></div>
                <div style="width:60%; margin-left:5%"><img width="100%" height="auto" preload="/assets/works/abc/imgs/lowRes/img6.png" src="/assets/works/abc/imgs/img6.png"></div>
            </div>
            <div class="cont">
                <div style="width:45%; margin-left:3%"><img width="100%" height="auto" preload="/assets/works/abc/imgs/lowRes/img4.png" src="/assets/works/abc/imgs/img4.png"></div>
                <div style="width:46%; margin-left:3%"><img width="100%" height="auto" preload="/assets/works/abc/imgs/lowRes/img5.png" src="/assets/works/abc/imgs/img5.png"></div>
            </div>
            <div class="cont">
                <div style="width:70%; margin-left:8%;"><div class="descr_cont">
                    <div class="title">Page produit et Page édition de produit.</div>
                    <div class="description">L'important était de faire une page d'édition intuitive et reprennant le principe de la page affichée aux visiteurs. Cela a permis de faciliter la prise en main du personnel d'ABC Diffusion.</div>
                </div></div>
            </div>
            <div class="cont">
                <div class="part_cont" style="margin-left:10%; width:30em;">
                    <div class="number">03.</div>
                    <div><div class="title">La base de données.</div>
                    <div class="description"></div>
                    </div>
                </div>
            </div>
            <div class="cont"><div style="width:80%; margin-left:12%"><img width="100%" height="auto" preload="/assets/works/abc/imgs/lowRes/img7.png" src="/assets/works/abc/imgs/img7.png"></div></div>
        </div>
        <div class="work_bottom_page">
            <div class="pre-next-proj">Prochain projet</div>
            <div id="next_project_trigger" class="hover-target" text-hover="NEXT" link-work="jee">Java Web.
            <div class="arrow_next"><i class="fas fa-arrow-right"></i></div></div>       
            <div class="txt_btm_right_GQO92">
                <div class="hover-target" text-hover="DL"><a target="_blank" href="/assets/files/Franck-Desfrancais-CV.pdf">Curriculum vitae</a></div>
                <div>·</div>    
                <div>© 2019 Franck Desfrançais.</div>
            </div>      
        </div>
    </div>
</body>