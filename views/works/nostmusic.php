<html>
<head>
<link rel="stylesheet" type="text/css" href="/assets/works/default/style.css">
<script src="/assets/works/default/script.js"></script>
<!--<link rel="stylesheet" type="text/css" href="/assets/works/nostmusic/style.css">-->
</head>

<body>
    <div class="work_scroll_full_100_100"><!--background color div --></div>
    <div class="work_scroll_full_100_100" id="QJGIO190" scrollbar>
        <div class="work_bk_100 hover-target" text-hover="SCROLL" style="background:#ffffff">
            <div class="work_semi_circle" style="display:none">
                <div class="semi_circle" style="transform:rotate(180deg)">
                    <svg class="size1" width="450" height="200" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <circle cx="225" cy="225" r="225" fill="white" clip-path="url(#cut-off-bottom)" />
                    </svg>
                    <svg class="size2" width="350" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <circle cx="175" cy="170" r="175" fill="white" clip-path="url(#cut-off-bottom)" />
                    </svg>
                </div>
            </div>
            <img class="bk" style="width:50%; max-width:80vh" preload="/assets/works/nostmusic/imgs/lowRes/logo.png" src="/assets/works/nostmusic/imgs/logo.png">
        </div>
        <div class="work_width_infos_page_80">
            <div data-smooth-from="-15px" data-smooth-to="25px" class="work_page_title">Nost<br>Portfolio.</div>
            <div class="work_main_infos_cont">
                <div class="left_cont">
                    <div class="infos_block">
                        <div class="title">Client</div>
                        <div>Nost</div>
                    </div>
                    <div class="infos_block">
                        <div class="title">Durée</div>
                        <div>3 semaines - 1 mois</div>
                        <div>2019</div>
                    </div>
                    <div class="infos_block">
                        <div class="title">Travail</div>
                        <div>Strategy, UX research,<br> concepts, messaging hierarchy, <br>web design (IA, IxD, design, UX/UI design, prototyping, animation), <br>TOV, Back developper, content definition.</div>
                    </div>
                </div>
                <div class="right_cont">Nost voulait un portfolio permettant à la fois de regrouper ses musiques et ses dessins (artworks, covers, logo, etc.).<br>
                Le site devait également être optimisé pour mobile, puisque la plupart des recherches se font aujourd'hui sur téléphone.<br>
                Finalement, le site devait être très design. J'ai donc choisi de le construire sans framework (pour plus de modularité des animations), et en limitant les rafraichissements de pages.
                <div class="link"><a class=" hover-target" href="//nostmusic.com" target="_blank">Voir le site.</a></div>
                </div>
            </div>
        </div>
        <div class="work_img_page_cont">
          <div class="cont">
              <div class="part_cont" style="margin-left:10%; width:35em;">
                  <div class="number">01.</div>
                  <div><div class="title">L'accueil.</div>
                  <div class="description"></div>
                  </div>
              </div>
          </div>
          <div class="cont">
              <div data-smooth-from="8em" data-smooth-to="0" style="width:70%; margin-left:10%;"><img width="100%" height="auto" preload="/assets/works/nostmusic/imgs/lowRes/img1.png" src="/assets/works/nostmusic/imgs/img1.png"></div>            
          </div>
          <div class="cont">
              <div style="width:70%; margin-left:10%; text-align:left;"><div class="descr_cont">
                  <div class="description">
                  La page d'accueil reprend tous les éléments importants que Nost souhaitait. On y retrouve tous les moyens de le contacter, et d'un simple scroll/swipe, on accède à toutes ses musiques et artworks.
                  </div>
              </div></div>
          </div>
          <div class="cont">
              <div class="part_cont" style="margin-left:10%; width:35em;">
                  <div class="number">02.</div>
                  <div><div class="title">La musique.</div>
                  <div class="description">Comment la page fonctionne ?</div>
                  </div>
              </div>
          </div>
          <div class="cont mobile_column_cont_1">
              <div style="width:55%; margin-left:7%;"><img width="100%" height="auto" preload="/assets/works/nostmusic/imgs/lowRes/img2.png" src="/assets/works/nostmusic/imgs/img2.png"></div> 
              <div style="width:25%; margin-left:5%;"><div data-pinned class="descr_cont">
                  <div class="title">Les éléments de la page.</div>
                  <div class="description">Cette page est alimentée par Soundcloud. Les musiques sont récupérées directement sur le profil de Nost. En haut de la page nous retrouvons les albums. Nous avons ensuite les musiques mises en avant (choisies par Nost), puis le reste de ses musiques.</div>
              </div></div>           
          </div>
          <div class="cont">
              <div class="part_cont" style="margin-left:calc(90% - 30em); width:30em;">
                  <div class="number">03.</div>
                  <div><div class="title">Les designs.</div>
                  <div class="description"></div>
                  </div>
              </div>
          </div>
          <div class="cont">
              <div style="width:80%; margin-left:0;"><img width="100%" height="auto" preload="/assets/works/nostmusic/imgs/lowRes/img3.png" src="/assets/works/nostmusic/imgs/img3.png"></div>            
          </div>
          <div class="cont">
              <div style="width:80%; margin-left:20%;"><img width="100%" height="auto" preload="/assets/works/nostmusic/imgs/lowRes/img4.png" src="/assets/works/nostmusic/imgs/img4.png"></div>            
          </div>
          <div class="cont">
              <div class="part_cont" style="margin-left:calc(80% - 25em); width:25em; text-align:right;">
                  <div class="number">04.</div>
                  <div><div class="title">Les covers.</div>
                  <div class="description">Page qui étend les designs.</div>
                  </div>
              </div>
          </div>
          <div class="cont">
              <div data-smooth-from="8em" data-smooth-to="-8em" style="width:20%; margin-left:7%;"><img width="100%" height="auto" preload="/assets/works/nostmusic/imgs/lowRes/img6.png" src="/assets/works/nostmusic/imgs/img6.png"></div>            
              <div style="width:65%; margin-left:5%;"><img width="100%" height="auto" preload="/assets/works/nostmusic/imgs/lowRes/img5.png" src="/assets/works/nostmusic/imgs/img5.png"></div>            
          </div>
          <div data-bk="rgb(0, 0, 0)" data-color="white"></div>
          <div class="cont">
              <div class="part_cont" style="margin-left:10%; width:30em;">
                  <div class="number">05.</div>
                  <div><div class="title">Les thèmes.</div>
                  <div class="description">Mode nuit / jour.</div>
                  </div>
              </div>
          </div>
          <div class="cont">
              <div style="width:100%; margin-left:0%;"><img width="100%" height="auto" preload="/assets/works/nostmusic/imgs/lowRes/img7.png" src="/assets/works/nostmusic/imgs/img7.gif"></div>            
          </div>
          <div class="cont">
              <div style="width:70%; margin-left:10%; text-align:left;"><div class="descr_cont">
                  <div class="description">
                  Que l'utilisateur soit en plein soleil ou au fond de son lit, dans le noir, il y a un thème pour profiter du portfolio.
                  </div>
              </div></div>
          </div>
          <div data-bk="rgba(247, 247, 247,1)" data-color="black"></div>
          <div class="cont">
              <div class="part_cont" style="margin-left:calc(90% - 30em); width:30em; text-align:right" data-smooth-from="-4em" data-smooth-to="8em">
                  <div class="number">06.</div>
                  <div><div class="title">Responsive.</div>
                  <div class="description">Un design pensé pour tous les écrans.</div>
                  </div>
              </div>
          </div>
          <div class="cont dir_column w_100_mobile">
              <div style="width:80%; margin-left:10%;"><img width="100%" height="auto" preload="/assets/works/nostmusic/imgs/lowRes/img8.png" src="/assets/works/nostmusic/imgs/img8.png"></div>            
              <div data-smooth-from="0" data-smooth-to="-3em" style="width:80%; margin-left:10%; margin-top:"><img width="100%" height="auto" preload="/assets/works/nostmusic/imgs/lowRes/img9.png" src="/assets/works/nostmusic/imgs/img9.png"></div>            
              <div style="width:25em; margin-left:calc(90% - 25em); text-align:center; font-size:0.9em; margin-right:2em"><div class="descr_cont">
                  <div class="description">
                  Template des mockups par <a class="hover-target" href="//anthonyboyd.graphics" target="_blank"><u>Anthony Boyd</u></a>
                  </div>
              </div></div>
          </div>
          <div class="cont">
              <div class="part_cont" style="margin-left:5%; width:90%; text-align:center; justify-content: center;"><a href="//nostmusic.com" target="_blank" class="hover-target">
                  <div class="number"></div>
                  <div><div class="title">Consulter le site.</div>
                  <div class="description">Cliquez pour accéder au site.</div>
                  </div></a>
              </div>
          </div>
        </div>
        <div class="work_bottom_page">
            <div class="pre-next-proj">Prochain projet</div>
            <div id="next_project_trigger" class="hover-target" text-hover="NEXT" link-work="notes-app">Notes App.
            <div class="arrow_next"><i class="fas fa-arrow-right"></i></div></div>       
            <div class="txt_btm_right_GQO92">
                <div class="hover-target" text-hover="DL"><a target="_blank" href="/assets/files/Franck-Desfrancais-CV.pdf">Curriculum vitae</a></div>
                <div>·</div>    
                <div>© 2019 Franck Desfrançais.</div>
            </div>      
        </div>
    </div>
</body>
</html>