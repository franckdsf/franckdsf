<html>
<head>
<link rel="stylesheet" type="text/css" href="/assets/works/default/style.css">
<script src="/assets/works/default/script.js"></script>
<!--<link rel="stylesheet" type="text/css" href="/assets/works/axopen/style.css">-->
</head>

<body>
    <div class="work_scroll_full_100_100"><!--background color div --></div>
    <div class="work_scroll_full_100_100" id="QJGIO190" scrollbar>
        <div class="work_bk_100 hover-target" text-hover="SCROLL" style="background:#0061CA">
            <div class="work_semi_circle">
                <div class="semi_circle" style="transform:rotate(180deg)">
                    <svg class="size1" width="450" height="200" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <circle cx="225" cy="225" r="225" fill="white" clip-path="url(#cut-off-bottom)" />
                    </svg>
                    <svg class="size2" width="350" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <circle cx="175" cy="170" r="175" fill="white" clip-path="url(#cut-off-bottom)" />
                    </svg>
                </div>
            </div>
            <img class="bk" style="width:50%; max-width:80vh" preload="/assets/works/axopen/imgs/lowRes/logo.png" src="/assets/works/axopen/imgs/logo.png">
        </div>
        <div class="work_width_infos_page_80">
            <div data-smooth-from="-15px" data-smooth-to="25px" class="work_page_title">Stage<br>Axopen.</div>
            <div class="work_main_infos_cont">
                <div class="left_cont">
                    <div class="infos_block">
                        <div class="title">Entreprise</div>
                        <div>Axopen</div>
                    </div>
                    <div class="infos_block">
                        <div class="title">Location</div>
                        <div>Lyon, Villeurbanne</div>
                    </div>
                    <div class="infos_block">
                        <div class="title">Travail</div>
                        <div>Developpeur Angular,<br>Developpeur Android,<br>Developpeur MongoDB.</div>
                    </div>
                    <div class="infos_block">
                        <div class="title">Mission</div>
                        <div>Manexi<br></div>
                    </div>
                    <div class="infos_block">
                        <div class="title">Documentations</div>
                        <div>
                        <a class="hover-target" target="_blank" href="/assets/works/axopen/files/Rapport-de-stage-Axopen.docx">Rapport de stage</a><br>
                        <a class="hover-target" target="_blank" href="/assets/works/axopen/files/Notice_d'utilisation-Manexi_back-office.docx">Documentation Utilisateur</a><br>
                        <a class="hover-target" target="_blank" href="/assets/works/axopen/files/Documentation-technique.docx">Documentation Technique</a>
                        </div>
                    </div>
                </div>
                <div class="right_cont">Durant ma deuxième année de bts, j'ai effectué un stage de 2 mois dans l'entreprise Axopen.<br>
                J'ai travaillé sur le projet de l'entreprise Manexi en temps que Développeur Android et Angular 2. J'avais pour mission l'amélioration et la correction de deux applications : Une back office (Angular) et une Android.
                <div class="link"><a class=" hover-target" href="/assets/works/axopen/files/competences.png" target="_blank">Voir les compétences.</a></div>
                </div>
            </div>
        </div>
        <div class="work_img_page_cont">
            <div class="cont"><div style="width:84%; margin-left:8%"><img width="100%" height="auto" preload="/assets/works/axopen/imgs/lowRes/img1.png" src="/assets/works/axopen/imgs/img1.png"></div></div>
            <div class="cont">
                <div class="part_cont" style="margin-left:10%; width:30em;">
                    <div class="number">01.</div>
                    <div><div class="title">Présentation.</div>
                    <div class="description">L'entreprise Axopen.</div>
                    </div>
                </div>
            </div>
            <div class="cont">
                <div style="width:70%; margin-left:12%;"><div class="descr_cont">
                    <div class="description">Axopen est une entreprise créée en 2007. Elle emploie aujourd'hui une trentaine de salariés. Elle est spécialisée dans la création et la maintenance d'applications pour d'autres entreprises.
                        <br>Elle compte parmi ses clients Edf, Manexi, Feu vert, Pegasus, ect.
                    </div>
                </div></div>
            </div>
            <div class="cont">
                <div class="part_cont" style="margin-left:calc(95% - 30em); width:30em; text-align:right;">
                    <div class="number">02.</div>
                    <div><div class="title">Ma mission.</div>
                    <div class="description">Le projet Manexi.</div>
                    </div>
                </div>
            </div>
            <div class="cont">
                <div style="width:70%; margin-left:20%; text-align:right;"><div class="descr_cont">
                    <div class="description">L'entreprise Manexi est rentrée en contact avec Axopen dans l'objectif de créér une application.<br>
                    Manexi étant spécialisée dans l'analyse des batîments, elle voulait une application permettant d'effectuer et de gérer ces analyses en fonction des batîments.
                    <br><br>En est résulté deux applications :<br>
                    Une application back office développée en Angular 2, permettant la création des formulaires d'analyses et la gestion des projets.<br>
                    Une application Android pour tablette, pour les employés sur le terrain afin de gérer les projets.
                    </div>
                </div></div>
            </div>
            <div class="cont">
                <div class="part_cont" style="margin-left:10%; width:30em;">
                    <div class="number">03.</div>
                    <div><div class="title">Back office.</div>
                    <div class="description">L'application back office est une application web nécessitant une connexion internet.</div>
                    </div>
                </div>
            </div>
            <div class="cont"><div style="width:80%; margin-left:12%"><img width="100%" height="auto" preload="/assets/works/axopen/imgs/lowRes/img2.png" src="/assets/works/axopen/imgs/img2.png"></div></div>
            <div class="cont">
                <div style="width:60%; margin-left:8%"><img width="100%" height="auto" preload="/assets/works/axopen/imgs/lowRes/img3.png" src="/assets/works/axopen/imgs/img3.png"></div>
                <div style="width:25%; margin-left:3%;"><div class="descr_cont">
                    <div class="title">Page projets.</div>
                    <div class="description">Cette page regroupe tous les projets (les différents batîments) sur lesquels l'entreprise Manexi travail.</div>
                </div></div>
            </div>
            <div class="cont">
                <div style="width:25%; margin-left:8%;"><div class="descr_cont">
                    <div class="title">Exemple de formulaire.</div>
                    <div class="description">Les formulaires composent les projets. Ils sont assignés aux différents murs des batîments.</div>
                </div></div>
                <div style="width:60%; margin-left:3%"><img width="100%" height="auto" preload="/assets/works/axopen/imgs/lowRes/img4.png" src="/assets/works/axopen/imgs/img4.png"></div>
            </div>
            <div class="cont mobile_column_cont_1">
                <div style="width:60%; margin-left:8%"><img width="100%" height="auto" preload="/assets/works/axopen/imgs/lowRes/img5.png" src="/assets/works/axopen/imgs/img5.png"></div>
                <div style="width:25%; margin-left:3%;"><div data-pinned class="descr_cont">
                    <div class="title">Edition d'un champ.</div>
                    <div class="description">Les champs composent les formulaires. Un champ peut être une question concernant un mur ou une zone,<br>
                    un tableau de valeurs, un prélevement d'échantillon de mur.
                    </div>
                </div></div>
            </div>
            <div data-bk="rgb(0,198,245)" data-color="white"></div>
            <div class="cont">
                <div class="part_cont" style="margin-left:10%; width:30em;">
                    <div class="number">04.</div>
                    <div><div class="title">Android.</div>
                    <div class="description">L'application android. Elle est développée pour les personnes sur le terrain. Elle fonctionne en hors ligne et en ligne.</div>
                    </div>
                </div>
            </div>
            <div class="cont"><div style="width:100%; margin-left:0%"><img width="100%" height="auto" preload="/assets/works/axopen/imgs/lowRes/img6.png" src="/assets/works/axopen/imgs/img6.png"></div></div>
            <div class="cont"><div style="width:80%; margin-left:8%" data-smooth-from="30px" data-smooth-to="-50px"><img width="100%" height="auto" preload="/assets/works/axopen/imgs/lowRes/img7.png" src="/assets/works/axopen/imgs/img7.png"></div></div>
            <div class="cont">
                <div style="width:70%; margin-left:12%;"><div class="descr_cont">
                    <div class="title">Plan d'un projet.</div>
                    <div class="description">Le plan comporte des balises positionnées sur les murs. Ces balises contiennent des formulaires.</div>
                </div></div>
            </div>
            <div class="cont"><div style="width:80%; margin-left:17%"><img width="100%" height="auto" preload="/assets/works/axopen/imgs/lowRes/img8.png" src="/assets/works/axopen/imgs/img8.png"></div></div>
            <div data-bk="rgba(255, 255, 255, 1)" data-color="black"></div>
            <div class="cont">
                <div style="width:70%; margin-left:20%; text-align:right;"><div class="descr_cont">
                    <div class="title">Formulaire d'une balise.</div>
                    <div class="description"></div>
                </div></div>
            </div>
        </div>
        <div class="work_bottom_page">
            <div class="pre-next-proj">Prochain projet</div>
            <div id="next_project_trigger" class="hover-target" text-hover="NEXT" link-work="abc">ABC diffusion.
            <div class="arrow_next"><i class="fas fa-arrow-right"></i></div></div>       
            <div class="txt_btm_right_GQO92">
                <div class="hover-target" text-hover="DL"><a target="_blank" href="/assets/files/Franck-Desfrancais-CV.pdf">Curriculum vitae</a></div>
                <div>·</div>    
                <div>© 2019 Franck Desfrançais.</div>
            </div>      
        </div>
    </div>
</body>