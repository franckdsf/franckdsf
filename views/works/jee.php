<html>
<head>
<link rel="stylesheet" type="text/css" href="/assets/works/default/style.css">
<script src="/assets/works/default/script.js"></script>
<!--<link rel="stylesheet" type="text/css" href="/assets/works/jee/style.css">-->
</head>

<body>
    <div class="work_scroll_full_100_100"><!--background color div --></div>
    <div class="work_scroll_full_100_100" id="QJGIO190" scrollbar>
        <div class="work_bk_100 hover-target" text-hover="SCROLL" style="background:#3AA0FF;">
            <div class="work_semi_circle">
                <div class="semi_circle" style="transform:rotate(90deg)">
                    <svg class="size1" width="450" height="200" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <circle cx="225" cy="225" r="225" fill="#96CDFF" clip-path="url(#cut-off-bottom)" />
                    </svg>
                    <svg class="size2" width="350" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <circle cx="175" cy="170" r="175" fill="#96CDFF" clip-path="url(#cut-off-bottom)" />
                    </svg>
                </div>
            </div>
            <img class="bk" style="width:30%" preload="/assets/works/jee/imgs/lowRes/logo.png" src="/assets/works/jee/imgs/logo.png">
        </div>
        <div class="work_width_infos_page_80">
            <div data-smooth-from="-15px" data-smooth-to="25px" class="work_page_title">Projet<br>Java web.</div>
            <div class="work_main_infos_cont">
                <div class="left_cont">
                    <div class="infos_block">
                        <div class="title">Type</div>
                        <div>PPE</div>
                    </div>
                    <div class="infos_block">
                        <div class="title">Localisation</div>
                        <div>Centre de formation</div>
                    </div>
                    <div class="infos_block">
                        <div class="title">Travail</div>
                        <div>                            
                            UX recherche, concepts,<br>
                            Web design (IA, IxD, design, UX/UI design, prototyping, animation),<br>
                            Back developpeur Java/MySQL,<br>
                            Front developpeur Html/Css/Jquery/Javascript,<br>
                            Database developpeur PhpMyAdmin,</div>
                    </div>
                    <div class="infos_block">
                        <div class="title">Documentations</div>
                        <div>
                        <a class="hover-target" target="_blank" href="/assets/works/jee/files/Documentation-technique.pdf">Documentation Technique</a>
                        </div>
                    </div>
                </div>
                <div class="right_cont"><div>La M2L (organisation fictive) souhaitait pouvoir associer des QCMs à ses cours et formations. L'application JEE permet de créer ces QCMs et d'y répondre avant d'afficher la correction.</div>
                <div class="link"><a class=" hover-target" href="/assets/works/jee/files/competences.png" target="_blank">Voir les compétences.</a></div>
                <!--<div class="link"><a class=" hover-target" href="" target="_blank">Voir le site.</a></div>-->
                </div>
            </div>
        </div>
        <div class="work_img_page_cont">
            <div data-bk="rgba(208, 0, 255, 1)" data-color="white"></div>
            <div class="cont">
                <div class="part_cont" style="margin-left:10%; width:30em;">
                    <div class="number">01.</div>
                    <div><div class="title">Le site.</div>
                    <div class="description">Galerie photos de l'application.</div>
                    </div>
                </div>
            </div>
            <div class="cont"><div style="width:80%; margin-left:12%"><img width="100%" height="auto" preload="/assets/works/jee/imgs/lowRes/img1.png" src="/assets/works/jee/imgs/img1.png"></div></div>
            <div class="cont">
                <div style="width:35%; margin-left:10%"><img width="100%" height="auto" preload="/assets/works/jee/imgs/lowRes/img2.png" src="/assets/works/jee/imgs/img2.png"></div>
                <div data-smooth-from="5em" data-smooth-to="0" style="width:45%; margin-left:4%; margin-top:-5%;"><img width="100%" height="auto" preload="/assets/works/jee/imgs/lowRes/img3.png" src="/assets/works/jee/imgs/img3.png"></div>            
            </div>
            <div class="cont mobile_column_cont_1">
                <div style="width:60%; margin-left:8%"><img width="100%" height="auto" preload="/assets/works/jee/imgs/lowRes/img5.png" src="/assets/works/jee/imgs/img5.png"></div>
                <div style="width:25%; margin-left:3%;"><div data-pinned class="descr_cont">
                    <div class="title">Page d'édition de Qcm.</div>
                    <div class="description">Un Qcm est composé de questions. Chaque question a une ou plusieurs réponses. Il est possible de mettre une ou plusieurs réponses justes. 
                    <br>Cette page développée pour une édition intuitive permet de modifier facilement les questions et d'ajouter ou supprimer en un clic les réponses.
                    </div>
                </div></div>
            </div>
            <div data-bk="rgba(247, 247, 247, 1)" data-color="black"></div>
            <div class="cont"><div style="width:80%; margin-left:12%"><img width="100%" height="auto" preload="/assets/works/jee/imgs/lowRes/img4.png" src="/assets/works/jee/imgs/img4.png"></div></div>
            <div class="cont">
                <div class="part_cont" style="margin-left:10%; width:30em;">
                    <div class="number">02.</div>
                    <div><div class="title">La base de données.</div>
                    <div class="description"></div>
                    </div>
                </div>
            </div>
            <div class="cont"><div style="width:80%; margin-left:12%"><img width="100%" height="auto" preload="/assets/works/jee/imgs/lowRes/img6.png" src="/assets/works/jee/imgs/img6.png"></div></div>
        </div>
        <div class="work_bottom_page">
            <div class="pre-next-proj">Prochain projet</div>
            <div id="next_project_trigger" class="hover-target" text-hover="NEXT" link-work="nostmusic">Nost portfolio.
            <div class="arrow_next"><i class="fas fa-arrow-right"></i></div></div>       
            <div class="txt_btm_right_GQO92">
                <div class="hover-target" text-hover="DL"><a target="_blank" href="/assets/files/Franck-Desfrancais-CV.pdf">Curriculum vitae</a></div>
                <div>·</div>    
                <div>© 2019 Franck Desfrançais.</div>
            </div>      
        </div>
    </div>
</body>