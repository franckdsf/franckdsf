<html>
<head>
<link rel="stylesheet" type="text/css" href="/assets/css/transitions.css">
</head>

<body>
    <div id="trans_page_opac_KOAPG9130"></div>
    <div id="trans_page_black_left_right_KOPE102" class="hover-target"></div>
    <div id="trans_page_loading_AOK102" class="hover-target"></div>
</body>
</html>

<script>
var transition_isAnimating = false;
function transition_LR(options){
    var defaults = {color:null,type:"in-out",prev:global_current_page,next:global_current_page};
    options = setDefaults_transition(options, defaults);

    if(options.color != null)
        $("#trans_page_black_left_right_KOPE102").css({"background":options.color});

    if(options.type.includes("in") && options.type.includes("out")){
        $("#trans_page_black_left_right_KOPE102").css({"width":0, "right":"auto", "left":"0"});
        $("#trans_page_black_left_right_KOPE102").animate({"width":"100%"},500);
        setTimeout(() => {
            $(options.prev.div).css({"display":"none"});
            $(options.next.div).css({"display":"block"});
            $("#trans_page_black_left_right_KOPE102").css({"right":"0", "left":"auto"});
            $("#trans_page_black_left_right_KOPE102").animate({"width":"0"},500);      
            assignPages(options.prev, options.next);      
        }, 1000);
    }
    else if(options.type.includes("out")){
        $(options.prev.div).css({"display":"none"});
        $(options.next.div).css({"display":"block"});
        $("#trans_page_black_left_right_KOPE102").css({"right":"0", "left":"auto"});
        $("#trans_page_black_left_right_KOPE102").animate({"width":"0"},500);
        $("#cursor").removeClass('loading');
        assignPages(options.prev, options.next);    
    }
    else if(options.type.includes("in")){
        $("#trans_page_black_left_right_KOPE102").css({"width":0, "right":"auto", "left":"0"});
        $("#trans_page_black_left_right_KOPE102").animate({"width":"100%"},500);
        $("#cursor").addClass('loading');
    }
}
function transition1(prev_page, next_page){
    if(transition_isAnimating)return;
    transition_isAnimating = true;

    var prev = prev_page.div;
    var next = next_page.div;
    if(prev_page.div.charAt(0) != "#")
        prev = "#"+prev_page.div;
    if(next_page.div.charAt(0) != "#")
        next = "#"+next_page.div;

    $(prev).css({
        'position':'fixed',
        'overflowY':'hidden',
        'height':'100%',
        'width':'100%',
        'top':'0',
        'bottom':'auto',
        'z-index':'99'
    });
    $("#trans_page_opac_KOAPG9130").css({'top':"0",'opacity':'0'});
    $("#trans_page_opac_KOAPG9130").animate({
        'top':'-50vh',
        'opacity':'0.5'
    },1000)
    $(prev).animate({
        'top':'-50vh',
    },1000);
    $(next).css({
        'display':'block',
        'position':'fixed',
        'overflowY':'hidden',
        'height':'100%',
        'width':'100%',
        'top':'100vh',
        'bottom':'auto',
        'z-index':'101'
    });
    $(next).animate({
        'top':'0'
    },1000,function(){
        $(prev).css('display','none');
        transition_isAnimating = false;
    });

    assignPages(prev_page, next_page);
}
function transitionToWork(prev_page, next_page){
    if(transition_isAnimating)return;
    transition_isAnimating = true;

    var prev = prev_page.div;
    var next = next_page.div;
    if(prev_page.div.charAt(0) != "#")
        prev = "#"+prev_page.div;
    if(next_page.div.charAt(0) != "#")
        next = "#"+next_page.div;

    $(prev).css({
        'position':'fixed',
        'overflowY':'hidden',
        'height':'100%',
        'width':'100%',
        'top':'0',
        'bottom':'auto',
        'z-index':'99'
    });
    $("#trans_page_opac_KOAPG9130").css({'top':"0",'opacity':'0'});
    $("#trans_page_opac_KOAPG9130").animate({
        'top':'-50vh',
        'opacity':'0.5'
    },1000)
    $(prev).animate({
        'top':'-50vh',
    },1000);
    $(next).css({
        'display':'block',
        'position':'fixed',
        'overflowY':'hidden',
        'height':'0',
        'width':'100%',
        'top':'auto',
        'bottom':'0',
        'z-index':'101'
    });
    $(next).animate({
        'height':'100%'
    },1000,function(){
        $(prev).css('display','none');
        transition_isAnimating = false;
    });

    assignPages(prev_page, next_page);
}
function transitionLoading(options){
    var defaults = {type:"in-out"};
    options = setDefaults_transition(options, defaults);

    if(options.type.includes("in") && options.type.includes("out")){
        $("#trans_page_loading_AOK102").fadeIn(300);
        $("#cursor").addClass('loading');
        setTimeout(() => {
            $("#trans_page_loading_AOK102").fadeOut(100);
            $("#cursor").removeClass('loading');    
        }, 1000);
    }
    else if(options.type.includes("in")){
        $("#trans_page_loading_AOK102").fadeIn(300);
        $("#cursor").addClass('loading');
    }
    else if(options.type.includes("out")){
        $("#trans_page_loading_AOK102").fadeOut(100);
        $("#cursor").removeClass('loading');
    }
}
</script>

<script>
function setDefaults_transition(options, defaults){
    return _.defaults({}, _.clone(options), defaults);
}
</script>