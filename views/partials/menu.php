<html>
<head>
<link rel="stylesheet" type="text/css" href="/assets/css/menu.css">
</head>

<body>
    <div class="menu_home_trigger_cont"><div class="attractHover_targetJOQG19"><div class="menu_home_trigger_button_parent"><div class="menu_home_trigger_button hover-target" text-hover="retour">Franck.</div></div></div></div>
    <div class="menu_contact_trigger_cont"><div class="attractHover_targetGODSP8"><div class="menu_contact_trigger_button_parent"><div class="menu_contact_trigger_button hover-target">Saluer</div></div></div></div>
    <div class="toggle_menu_trigger_cont"><div class="attractHover_targetAKOP8">
        <div id="toggle_menu_button" class="hover-target">
            <div class="parent">
                <div></div>
                <div></div>
            </div>
        </div>
    </div></div>

    <div id="menu_full_100_100">
        <div class="flex_cont">
            <div class="left_cont">
                <div class="page_cont hover-target"  link-page="index">
                    <div class="page_number_cont"><div class="number">01.</div></div>
                    <div class="page_name">Index.</div>
                </div>
                <div class="page_cont hover-target"  link-page="works">
                    <div class="page_number_cont"><div class="number">02.</div></div>
                    <div class="page_name">Travaux.</div>
                </div>
                <div class="page_cont hover-target"  link-page="about">
                    <div class="page_number_cont"><div class="number">03.</div></div>
                    <div class="page_name">A propos.</div>
                </div>
                <div class="page_cont hover-target"  link-page="contact">
                    <div class="page_number_cont"><div class="number">04.</div></div>
                    <div class="page_name">Contact.</div>
                </div>
            </div>
            <div class="right_cont">
                <div class="link_name hover-target">Mail.</div>
                <div class="link_name hover-target"><a href="/assets/files/Franck-Desfrancais-CV.pdf" target="_blank">Curriculum vitae.</a></div>
                <div class="link_name hover-target"><a href="/assets/files/tableau_synthese.pdf" target="_blank">Tableau de Synthèse.</a></div>
            </div>
        </div>
    </div>
</body>

<script>
$(document).ready(function(){
    $.attractHover(
        '.attractHover_targetJOQG19',
        {
        proximity: 75,
        magnetism: 4
        }
    );
    $.attractHover(
        '.attractHover_targetGODSP8',
        {
        proximity: 75,
        magnetism: 4
        }
    );
    $.attractHover(
        '.attractHover_targetAKOP8',
        {
        proximity: 75,
        magnetism: 4
        }
    );

    //Click
    $(".menu_home_trigger_button").click(function(){
        if (global_current_page.div != global_previous_page.div){
            transition1(global_current_page, global_previous_page);
        }
        else if(global_current_page.page_name == "work"){
           transition1(global_current_page, global_works_page);
        }
    });
    $(".menu_contact_trigger_button").click(function(){
        if (global_current_page.div != global_contact_page.div){
            transition1(global_current_page, global_contact_page);
        }
    });
    $("#menu_full_100_100 .page_cont").click(function(){
        var next_page = null;
        if($(this).attr('link-page') == "index"){
            next_page = global_index_page;
        }
        else if($(this).attr('link-page') == "works"){
            next_page = global_works_page;
        }
        else if($(this).attr('link-page') == "about"){
            next_page = global_about_page;
        }
        else if($(this).attr('link-page') == "contact"){
            next_page = global_contact_page;
        }

        if(next_page.div != global_current_page.div){
            transition1(global_current_page, next_page);
        }
        closeMenu();
    });

    var isOpen = false;
    $("#toggle_menu_button").click(function(){
        if(isOpen){
            closeMenu();
        }
        else{
            openMenu();
        }
    });
    $(document).keyup(function(e) {
         if (e.keyCode === 27){
            if(isOpen){
                closeMenu();
            }
            else{
                openMenu();
            }
         }
    });
    function openMenu(){
        //init
        $("#menu_full_100_100 .right_cont").css({'top':'-25px','opacity':0.4});
        $("#menu_full_100_100 .page_cont").css({'top':'-3em','opacity':0});
        //anim
        $("#menu_full_100_100").animate({height:"100%"},{duration:500});
        $("#menu_full_100_100 .right_cont").animate({top:0, opacity:1},{duration:1000});

        timer = 0;
        $("#menu_full_100_100").find(".page_cont").each(function(){
            setTimeout(() => {
                 $(this).animate({top:0, opacity:1},{duration:1000});
            }, timer);
            timer += 100;
        });

        $("#toggle_menu_button").toggleClass("close");
        isOpen = !isOpen;
    }

    function closeMenu(){
        //anim
        timer = 0;
        $("#menu_full_100_100").find(".page_cont").each(function(){
            setTimeout(() => {
                $(this).animate({top:"-5em", opacity:0},{duration:400});
            }, timer);
            timer += 100;
        })
        $("#menu_full_100_100 .right_cont").animate({opacity:0},{duration:1000});

        setTimeout(() => {
            $("#menu_full_100_100").animate({height:"0"},{duration:400});
            $("#toggle_menu_button").toggleClass("close");
            isOpen = false;
        }, 500);        
    }
});
</script>