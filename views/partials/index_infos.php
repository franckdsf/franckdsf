<html>
<head>
<link rel="stylesheet" type="text/css" href="/assets/css/index_infos.css">
</head>

<body>
    <div class="fixed_100_100_infos">
        <div class="txt_abs_50_50_afopk hover-target" text-hover="voir">Voir plus.</div>
        <div class="txt_btm_right_GQO92">
            <div class="hover-target" text-hover="DL"><a target="_blank" href="/assets/files/Franck-Desfrancais-CV.pdf">Curriculum vitae</a></div>
            <div>·</div>    
            <div>© 2019 Franck Desfrançais.</div>
        </div>
    </div>
</body>

<script>
$(document).ready(function(){
    //Click home
    $(".txt_abs_50_50_afopk").click(function(){
        transition1(global_current_page, global_works_page);
    });
});
</script>