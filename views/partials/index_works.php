<html>
<head>
<link rel="stylesheet" type="text/css" href="/assets/css/index_works.css">
</head>

<body>
    <div class="index_swiper_elems_cont">
        <div class="four_corners_page_indicator_cont">
            <div class="bottom_corner corner"></div>
            <div class="right_corner corner"></div>
            <div class="top_corner_left corner"></div>
            <div class="top_corner_right corner"></div>
            <div class="left_corner corner"></div>
        </div>
        <div class="index_swiper_semi_circle" id="index_main_circle_OFAKP19">
            <svg class="size1" width="180" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <circle cx="90" cy="165" r="90" fill="#FFDA99" clip-path="url(#cut-off-bottom)" />
            </svg>
            <svg class="size2" width="240" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <circle cx="120" cy="160" r="120" fill="#FFDA99" clip-path="url(#cut-off-bottom)" />
            </svg>
        </div>
        <div class="index_swiper_cont">
            <div class="index_swiper_div" circle-color="#A9A9A9" link-work="nostmusic">
                <div class="_left_block hover-target">
                    <div class="elements">
                        <div class="bk" style="background:#fffff"></div>
                        <div class="index_swiper_semi_circle" demi-circle="180" style="display:none">
                            <svg class="size1" width="180" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <circle cx="90" cy="165" r="90" fill="#ffffff" clip-path="url(#cut-off-bottom)" />
                            </svg>
                            <svg class="size2" width="240" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <circle cx="120" cy="160" r="120" fill="#ffffff" clip-path="url(#cut-off-bottom)" />
                            </svg>
                        </div>
                        <div class="img" style="width:70%;filter:invert(100)"><img src="/assets/works/nostmusic/imgs/logo.png" width="100%"></div>
                        <div class="sub_text index_title_swiper"><div>Nostmusic</div><div>Portfolio.</div></div>
                    </div>
                </div>
                <div class="_right_block">
                    <div class="index_title_swiper hover-target"><div>Nostmusic</div><div>Portfolio.</div></div>
                    <div class="subtitle">Projet<div class="bar"></div>fin mars 2019 - debut avril 2019.</div>
                </div>
            </div>
            <div class="index_swiper_div" circle-color="#4F99E9" link-work="axopen">
                <div class="_left_block hover-target">
                    <div class="elements">
                        <div class="bk" style="background:#0061CA"></div>
                        <div class="index_swiper_semi_circle" demi-circle="90">
                            <svg class="size1" width="180" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <circle cx="90" cy="165" r="90" fill="white" clip-path="url(#cut-off-bottom)" />
                            </svg>
                            <svg class="size2" width="240" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <circle cx="120" cy="160" r="120" fill="white" clip-path="url(#cut-off-bottom)" />
                            </svg>
                        </div>
                        <div class="img" style="width:70%"><img src="/assets/works/axopen/imgs/logo.png" width="100%"></div>
                        <div class="sub_text index_title_swiper"><div>Stage</div><div>Axopen.</div></div>
                    </div>
                </div>
                <div class="_right_block">
                    <div class="index_title_swiper hover-target"><div>Stage</div><div>Axopen.</div></div>
                    <div class="subtitle">Stage de deux mois<div class="bar"></div> 2019.</div>
                </div>
            </div>
            <div class="index_swiper_div" circle-color="#BB004A" link-work="abc">
                <div class="_left_block hover-target">
                    <div class="elements">
                        <div class="bk" style="background:#942775"></div>
                        <div class="index_swiper_semi_circle" demi-circle="30">
                            <svg class="size1" width="180" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <circle cx="90" cy="165" r="90" fill="#BB004A" clip-path="url(#cut-off-bottom)" />
                            </svg>
                            <svg class="size2" width="240" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <circle cx="120" cy="160" r="120" fill="#BB004A" clip-path="url(#cut-off-bottom)" />
                            </svg>
                        </div>
                        <div class="img" style="width:70%"><img src="/assets/works/abc/imgs/logo.png" width="100%"></div>
                        <div class="sub_text index_title_swiper"><div>ABC</div><div>Diffusion.</div></div>
                    </div>
                </div>
                <div class="_right_block">
                    <div class="index_title_swiper hover-target"><div>ABC</div><div>Diffusion.</div></div>
                    <div class="subtitle">Stage de 6 semaines<div class="bar"></div> 2018.</div>
                </div>
            </div>
        </div>
        <div class="index_page_indicator_swiper">
            <div class="cur_page">01.</div>
            <div class="bar"><div id="index_bar_indicator_1290FAJL" class="bar-indicator"></div></div>
            <div class="total_page">04.</div>
        </div>
    </div>
</body>


<!-- pages - interactions -->
<script>
$(document).ready(function(){
    $('.index_swiper_div').click(function(){
        var link = $(this).attr('link-page');
        if(link != null){
            openExternalPage(link);
        }
        
        link = $(this).attr('link-work');
        if(link == null){
            return;
        }

        transitionLoading({type:"in"});
        $("#page-work").load("/openwork/"+link, function() {
            var next_page = {div:"page-work",page_name:"work",url:"/work/"+link};
            transitionToWork(global_current_page,next_page);
            transitionLoading({type:"out"});
        });
    });

    function openExternalPage(link){
        window.open("http://"+link);
        e.preventDefault(); //or return false;
    }
});
</script>

<!-- //Animations -->
<script>
function pourcent_mobile(){
    var pourcent = curSwiper*300/nbSwipers;
    var coloor = $(".index_swiper_div:nth-child("+(curSwiper)+")").attr('circle-color');
    $(".four_corners_page_indicator_cont > .bottom_corner").css({'width':pourcent+'%', 'background':coloor});
    $(".four_corners_page_indicator_cont > .right_corner").css({'height':(pourcent-100)+'%', 'background':coloor});
    $(".four_corners_page_indicator_cont > .left_corner").css({'height':(pourcent-100)+'%', 'background':coloor});
    $(".four_corners_page_indicator_cont > .top_corner_left").css({'width':(pourcent-200)/1.95+'%', 'background':coloor});
    $(".four_corners_page_indicator_cont > .top_corner_right").css({'width':(pourcent-200)/1.95+'%', 'background':coloor});
    
    if(curSwiper > nbSwipers)
        $(".four_corners_page_indicator_cont > *").css({opacity:0});
    else
        $(".four_corners_page_indicator_cont > *").css({opacity:1});
}

function swiper_out_prev(div){
    //init
    var timeout = 0;
    $(div).css({"align-items":"flex-end"});
    $(div).find("._left_block").css({"height":"100%"});
    $(div).find("._left_block > .elements").css({"top":"auto","bottom":"0"});
    //animate
    $(div).find('._right_block > .index_title_swiper').children().each(function(){
        setTimeout(() => {
            $(this).css('position','relative');
            $(this).animate({"top":"-30px","opacity":"0"},250);
        }, timeout+300);
        timeout += 200;
    });
    $(div).find("._right_block > .subtitle").animate({"top":"15%","opacity":0},1000);
    $(div).find("._left_block > .elements > .img").animate({"top":"80%"},500);
    $(div).find("._left_block").animate({"height":0},{duration:400,specialEasing: {height: "swing"}});
    
    setTimeout(() => {
        $(div).css('display','none');
    }, 1100);
}
function swiper_in_prev(div){
    //init
    var timeout = 0;
    $(div).css('display','flex');
    $(div).css({"align-items":"flex-start"});
    $(div).find("._left_block").css({"height":"0%"});
    $(div).find("._left_block > .elements").css({"top":0,"bottom":"auto"});
    $(div).find("._right_block > .subtitle").css({"top":"30%","opacity":0});
    $(div).find("._left_block > .elements > .img").css({"top":"40%"});
    $(div).find('._right_block > .index_title_swiper').children().each(function(){
        $(this).css({'position':'relative',"top":"30px","opacity":"0"});
    });
    //animate
    $(div).find('._right_block > .index_title_swiper').children().each(function(){
        setTimeout(() => {
            $(this).animate({"top":"0px","opacity":"1"},250);
        }, timeout+00);
        timeout += 200;
    });
    setTimeout(() => {$(div).find("._right_block > .subtitle").animate({"top":"26%","opacity":1},600);}, 300);
    $(div).find("._left_block > .elements > .img").animate({"top":"50%"},1000);
    setTimeout(() => {$(div).find("._left_block").animate({"height":"100%"},500);}, 300);
}
function swiper_out_next(div){
    //init
    var timeout = 0;
    $(div).css({"align-items":"flex-start"});
    $(div).find("._left_block").css({"height":"100%"});
    $(div).find("._left_block > .elements").css({"top":0,"bottom":"auto"});
    //animate
    $(div).find('._right_block > .index_title_swiper').children().each(function(){
        setTimeout(() => {
            $(this).css('position','relative');
            $(this).animate({"top":"-30px","opacity":"0"},250);
        }, timeout+300);
        timeout += 200;
    });
    $(div).find("._right_block > .subtitle").animate({"top":"15%","opacity":0},1000);
    $(div).find("._left_block > .elements > .img").animate({"top":"20%"},500);
    $(div).find("._left_block").animate({"height":0},{duration:400,specialEasing: {height: "swing"}});

    setTimeout(() => {
        $(div).css('display','none');
    }, 1100);
}
function swiper_in_next(div){
    //init
    var timeout = 0;
    $(div).css('display','flex');
    $(div).css({"align-items":"flex-end"});
    $(div).find("._left_block").css({"height":"0"});
    $(div).find("._left_block > .elements").css({"bottom":0,"top":"auto"});
    $(div).find("._right_block > .subtitle").css({"top":"30%","opacity":0});
    $(div).find("._left_block > .elements > .img").css({"top":"70%"});
    $(div).find('._right_block > .index_title_swiper').children().each(function(){
        $(this).css({'position':'relative',"top":"30px","opacity":"0"});
    });
    //animate
    $(div).find('._right_block > .index_title_swiper').children().each(function(){
        setTimeout(() => {
            $(this).animate({"top":"0px","opacity":"1"},250);
        }, timeout+00);
        timeout += 200;
    });
    setTimeout(() => {$(div).find("._right_block > .subtitle").animate({"top":"26%","opacity":1},600);}, 300);
    $(div).find("._left_block > .elements > .img").animate({"top":"50%"},1000);
    setTimeout(() => {$(div).find("._left_block").animate({"height":"100%"},500);}, 300);
}
function index_semi_circle_rotate(deg){
    $('.index_swiper_semi_circle').children().each(function(){
        $(this).css({"transform":"rotate("+deg+"deg) translateY(-50%)"});
    });
}
$(document).ready(function(){
    //Variables are now in main index.php page
    /*var curSwiper = 1;
    var nbSwipers = $(".index_swiper_cont").children().length;;
    var projects_isAnimating = false;*/

    $("#index_bar_indicator_1290FAJL").css('height',((curSwiper)*100/nbSwipers)+'%');
    index_semi_circle_rotate($(".index_swiper_div:nth-child("+(curSwiper)+")").find(".index_swiper_semi_circle").attr('demi-circle'));
    swiper_in_next(".index_swiper_div:nth-child("+(curSwiper)+")");
    $(".index_page_indicator_swiper").find(".cur_page").html("0"+curSwiper+".");
    $(".index_page_indicator_swiper").find(".total_page").html("0"+nbSwipers+".");

    $(".index_rel_100_100").bind('mousewheel DOMMouseScroll', function(e){
		if(projects_isAnimating)return;

        if((e.originalEvent.wheelDelta > 0 || e.originalEvent.detail < 0) && curSwiper > 0) {
            curSwiper--;
            //Block triggers mousewheel during animation
            projects_isAnimating = true;
            setTimeout(() => {
                projects_isAnimating=false;
                //Change color of main disk
                $("#index_main_circle_OFAKP19").find("[fill]").each(function(){
                    $(this).attr('fill',$(".index_swiper_div:nth-child("+(curSwiper)+")").attr('circle-color'));
                });
            }, 2300);
            
            $("#index_bar_indicator_1290FAJL").css('height',((curSwiper)*100/nbSwipers)+'%');
            //If i want to leave proj page
            if(curSwiper == 0)
                index_semi_circle_rotate($(".index_swiper_div:nth-child("+(1)+")").find(".index_swiper_semi_circle").attr('demi-circle')-70);
            else{
                index_semi_circle_rotate($(".index_swiper_div:nth-child("+(curSwiper)+")").find(".index_swiper_semi_circle").attr('demi-circle'));
                $(".index_page_indicator_swiper").find(".cur_page").html("0"+(curSwiper)+".");
            }

            //Animate Divs
            swiper_out_prev(".index_swiper_div:nth-child("+(curSwiper+1)+")");
            setTimeout(() => {
                swiper_in_prev(".index_swiper_div:nth-child("+(curSwiper)+")");
            }, 1000);
        }
        else if(((e.originalEvent.wheelDelta < 0 || e.originalEvent.detail > 0)) && curSwiper <= nbSwipers){
            curSwiper++;
            //Block triggers mousewheel during animation
            projects_isAnimating = true;
            setTimeout(() => {
                projects_isAnimating=false;
                //Change color of main disk
                $("#index_main_circle_OFAKP19").find("[fill]").each(function(){
                    $(this).attr('fill',$(".index_swiper_div:nth-child("+(curSwiper)+")").attr('circle-color'));
                });
            }, 2300);

            $("#index_bar_indicator_1290FAJL").css('height',((curSwiper)*100/nbSwipers)+'%');
            if(curSwiper > nbSwipers)
                index_semi_circle_rotate($(".index_swiper_div:nth-child("+(nbSwipers)+")").find(".index_swiper_semi_circle").attr('demi-circle')-70);
            else{
                $(".index_page_indicator_swiper").find(".cur_page").html("0"+(curSwiper)+".");
                index_semi_circle_rotate($(".index_swiper_div:nth-child("+(curSwiper)+")").find(".index_swiper_semi_circle").attr('demi-circle'));
            }
            
            //Animate Divs
            swiper_out_next(".index_swiper_div:nth-child("+(curSwiper-1)+")");
            setTimeout(() => {
                swiper_in_next(".index_swiper_div:nth-child("+(curSwiper)+")");
            }, 1000);
        }
        pourcent_mobile();
    });
    $("body").on('touchstart', function(e) {
        if(projects_isAnimating)return;

	    var swipe = e.originalEvent.touches,
	    start = swipe[0].pageY;

	    $(this).on('touchmove', function(e) {
            if(projects_isAnimating)return;

	        var contact = e.originalEvent.touches,
	        end = contact[0].pageY,
	        distance = end-start;

	        if (distance > 100 && curSwiper > 0) {
                curSwiper--;
                //Block triggers mousewheel during animation
                projects_isAnimating = true;
                setTimeout(() => {
                    projects_isAnimating=false;
                    //Change color of main disk
                    $("#index_main_circle_OFAKP19").find("[fill]").each(function(){
                        $(this).attr('fill',$(".index_swiper_div:nth-child("+(curSwiper)+")").attr('circle-color'));
                    });
                }, 1800);
                
                $("#index_bar_indicator_1290FAJL").css('height',((curSwiper)*100/nbSwipers)+'%');
                //If i want to leave proj page
                if(curSwiper == 0)
                    index_semi_circle_rotate($(".index_swiper_div:nth-child("+(1)+")").find(".index_swiper_semi_circle").attr('demi-circle')-70);
                else{
                    index_semi_circle_rotate($(".index_swiper_div:nth-child("+(curSwiper)+")").find(".index_swiper_semi_circle").attr('demi-circle'));
                    $(".index_page_indicator_swiper").find(".cur_page").html("0"+(curSwiper)+".");
                }

                //Animate Divs
                swiper_out_prev(".index_swiper_div:nth-child("+(curSwiper+1)+")");
                setTimeout(() => {
                    swiper_in_prev(".index_swiper_div:nth-child("+(curSwiper)+")");
                }, 1000);
            }
	        if (distance < -100  && curSwiper <= nbSwipers){
                curSwiper++;
                //Block triggers mousewheel during animation
                projects_isAnimating = true;
                setTimeout(() => {
                    projects_isAnimating=false;
                    //Change color of main disk
                    $("#index_main_circle_OFAKP19").find("[fill]").each(function(){
                        $(this).attr('fill',$(".index_swiper_div:nth-child("+(curSwiper)+")").attr('circle-color'));
                    });
                }, 1800);

                $("#index_bar_indicator_1290FAJL").css('height',((curSwiper)*100/nbSwipers)+'%');
                if(curSwiper > nbSwipers)
                    index_semi_circle_rotate($(".index_swiper_div:nth-child("+(nbSwipers)+")").find(".index_swiper_semi_circle").attr('demi-circle')-70);
                else{
                    $(".index_page_indicator_swiper").find(".cur_page").html("0"+(curSwiper)+".");
                    index_semi_circle_rotate($(".index_swiper_div:nth-child("+(curSwiper)+")").find(".index_swiper_semi_circle").attr('demi-circle'));
                }
                
                //Animate Divs
                swiper_out_next(".index_swiper_div:nth-child("+(curSwiper-1)+")");
                setTimeout(() => {
                    swiper_in_next(".index_swiper_div:nth-child("+(curSwiper)+")");
                }, 1000);
            }
            pourcent_mobile();
	    })
	    .one('touchend', function() {
	        $(this).off('touchmove touchend');
	    });
	});
});
</script>