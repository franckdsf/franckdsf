<html>
<head>
<link rel="stylesheet" type="text/css" href="/assets/css/first_visit.css">
</head>

<body>
    <div class="first_visit_full">
        <div class="first_visit_cont">
            <div class="countdown_cont">
                <div class="countdown_background">0%</div>
                <div class="ct_loader_cont">
                  <div class="countdown">0%</div>
                </div>
            </div>
            <div class="bottom_infos">
                <div><span>01.</span> Bouton Retour.</div>
                <div><span>02.</span> Bouton Menu (Echap).</div>
                <div><span>03.</span> Bouton Contact.</div>
            </div>
        </div>
    </div>
    <div class="first_visit_full" style="z-index:9999"></div>
</body>

<script>
$(document).ready(function(){
    var loading = 0;
    var height = 0.4;
    var timer = 300;
    
    if(!$.cookie("first_visit")){
        $(".first_visit_cont").css({height:window.innerHeight});
        $(".first_visit_full").css({height:window.innerHeight});
        $.cookie("first_visit", "false", { expires: 7 });
        load();
    }else{
        $(".first_visit_full").remove();
    }
    
    function load(){
        timer *= 0.95;
        loading += Math.floor(Math.random() * Math.floor(4))+1;
        
        if(loading > 100)
            loading = 100;
        
        height = 0.35+(loading*0.7/100);
        
        $('.countdown_background').html(loading+"%");
        $('.countdown').html(loading+"%");
        $('.ct_loader_cont').css({height:height+"em"});
        
        if(loading < 100)
            setTimeout(function(){load();}, timer);
        else
            $(".first_visit_full").animate({height:0},500,function(){this.remove();});
    }
});
</script>