<html>
<head>
<link rel="stylesheet" type="text/css" href="/assets/css/index.css">
</head>

<script>
    var curSwiper = 0;
    var nbSwipers = 1;
    var projects_isAnimating = false;

$(document).ready(function(){
    nbSwipers = $(".index_swiper_cont").children().length;
    $("#index_welcome_GOPK912").css('transition','margin-top ease-in-out 650ms');

    $(".index_rel_100_100").bind('mousewheel DOMMouseScroll', function(e){
        if(projects_isAnimating)return;
        var windowHeight = window.innerHeight;
        if((e.originalEvent.wheelDelta > 0 || e.originalEvent.detail < 0)) {
            //if i'm on "see more" and I want to get back to "works"
            if(curSwiper > nbSwipers){
                $("#index_welcome_GOPK912").css({marginTop:-windowHeight});    
            }
            //if i'm already on "welcome", return.
            if(curSwiper <= 0){
                return;
            }
            //if i'm on "works" and I want to get back to
            //Welcome partial, wait 750ms and animte
            setTimeout(() => {
                if(curSwiper <= 0)
                    $("#index_welcome_GOPK912").css({marginTop:"0"});    
            }, 750);
        }
        else if(((e.originalEvent.wheelDelta < 0 || e.originalEvent.detail > 0))){
            //if i'm not on "works" yet, animate to this partial
            if(curSwiper <= 0){
                $("#index_welcome_GOPK912").css({marginTop:-windowHeight}); 
            }
            //If i'm already on "see more", return.
            if(curSwiper > nbSwipers){
                return;
            }
            //Wait for 750ms and check if user is at the end of the works partial
            //If he is, then animate to "see more"
            setTimeout(() => {
                if(curSwiper > nbSwipers)
                    $("#index_welcome_GOPK912").css({marginTop:-windowHeight*2});    
            }, 750);
        }
    });
    $(".index_rel_100_100").on('touchstart', function(e) {
        var swipe = e.originalEvent.touches,
	    start = swipe[0].pageY;
        var windowHeight = window.innerHeight;

	    $(this).on('touchmove', function(e) {
            var contact = e.originalEvent.touches,
	        end = contact[0].pageY,
	        distance = end-start;

	        if (distance > 100) {
                if(curSwiper <= 0){
                    setTimeout(() => {
                        $("#index_welcome_GOPK912").css({marginTop:"0"});
                    }, 750);
                }else if(curSwiper <= nbSwipers){
                    $("#index_welcome_GOPK912").css({marginTop:-windowHeight});    
                }   
            }
	        else if (distance < -100){
                if(curSwiper <= nbSwipers){
                    if(curSwiper == 0){
                        $("#index_welcome_GOPK912").css({marginTop:-windowHeight});      
                    } 
                }
                else{
                    setTimeout(() => {
                        $("#index_welcome_GOPK912").css({marginTop:-windowHeight*2});
                    }, 750);
                }
            }
	    })
	    .one('touchend', function() {
	        $(this).off('touchmove touchend');
	    });
	});
});
</script>

<body>
    <div class="index_rel_100_100">
        <div id="index_infos_GOPK912">
            <?php require($_SERVER['DOCUMENT_ROOT']."/views/partials/index_infos.php"); ?>
        </div>    
        <div id="index_welcome_GOPK912">
            <?php require($_SERVER['DOCUMENT_ROOT']."/views/partials/index_welcome.php"); ?>
        </div>
        <div id="index_projects_GOPK912">
            <?php require($_SERVER['DOCUMENT_ROOT']."/views/partials/index_works.php"); ?>
        </div>
    </div>
</body>