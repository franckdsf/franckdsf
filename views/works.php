<html>
<head>
<link rel="stylesheet" type="text/css" href="/assets/css/works.css">
</head>

<body>
<div id="works_page_21Y8J03">
    <div class="works_flex">
        <div link-work="nostmusic" class="work_cont hover-target" style="margin-left:5vw; margin-right:7vw; margin-top:2vw;">    
            <div class="work_img_cont_parent"><div class="work_img_cont">
                <div class="work_bk" style="background:#fffff;">
                    <div class="work_semi_circle" style="display:none">
                        <div class="semi_circle" style="transform:rotate(90deg)">
                            <svg class="size1" width="250" height="120" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <circle cx="125" cy="125" r="125" fill="#ffffff" clip-path="url(#cut-off-bottom)" />
                            </svg>
                            <svg class="size2" width="180" height="90" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <circle cx="90" cy="90" r="90" fill="#ffffff" clip-path="url(#cut-off-bottom)" />
                            </svg>
                        </div>
                    </div>
                    <img class="img" src="/assets/works/nostmusic/imgs/logo.png" style="filter:invert(100)">
                </div>
            </div></div>
            <div class="sub_title">Nost music.</div>
            <div class="title">Portfolio.</div>
        </div>
        <div link-work="notes-app" class="work_cont hover-target" data-smooth-from="-200px" data-smooth-to="200px">  <div class="work_img_cont_parent"><div class="work_img_cont">
                <div class="work_bk" style="background:#FF8734;">
                    <div class="work_semi_circle" style="display:none">
                        <div class="semi_circle" style="transform:rotate(90deg)">
                            <svg class="size1" width="250" height="120" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <circle cx="125" cy="125" r="125" fill="#ffffff" clip-path="url(#cut-off-bottom)" />
                            </svg>
                            <svg class="size2" width="180" height="90" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <circle cx="90" cy="90" r="90" fill="#ffffff" clip-path="url(#cut-off-bottom)" />
                            </svg>
                        </div>
                    </div>
                    <img class="img" style="width:60%" src="/assets/works/notes-app/imgs/logo.png">
                </div>
            </div></div>
            <div class="sub_title">App client lourd<div class="bar"></div> Electron & VueJS.</div>
            <div class="title">Notes app.</div>
        </div>
        <div link-work="kronz" class="work_cont hover-target" style="margin-right:7em;">    
            <div class="work_img_cont_parent"><div class="work_img_cont">
                <div class="work_bk" style="background:#F0F0F0;">
                    <div class="work_semi_circle">
                        <div class="semi_circle" style="transform:rotate(90deg)">
                            <svg class="size1" width="250" height="120" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <circle cx="125" cy="125" r="125" fill="#FFFFFF" clip-path="url(#cut-off-bottom)" />
                            </svg>
                            <svg class="size2" width="180" height="90" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <circle cx="90" cy="90" r="90" fill="#FFFFFF" clip-path="url(#cut-off-bottom)" />
                            </svg>
                        </div>
                    </div>
                    <img class="img" style="width:80%" src="/assets/works/kronz/imgs/logo.png">
                </div>
            </div>
            </div>
            <div class="sub_title">Travail personnel<div class="bar"></div> Dec 2018 - Fev 2019.</div>
            <div class="title">Kronz store.</div>
        </div>
        <div link-work="axopen" class="work_cont hover-target" data-smooth-from="80px" data-smooth-to="-80px" style="margin-right:10vw; margin-left:5vw;">    
            <div class="work_img_cont_parent"><div class="work_img_cont">
                <div class="work_bk" style="background:#0061CA;">
                    <div class="work_semi_circle">
                        <div class="semi_circle" style="transform:rotate(180deg)">
                            <svg class="size1" width="250" height="120" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <circle cx="125" cy="125" r="125" fill="white" clip-path="url(#cut-off-bottom)" />
                            </svg>
                            <svg class="size2" width="180" height="90" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <circle cx="90" cy="90" r="90" fill="white" clip-path="url(#cut-off-bottom)" />
                            </svg>
                        </div>
                    </div>
                    <img class="img" style="width:60%" src="/assets/works/axopen/imgs/logo.png">
                </div>
            </div></div>
            <div class="sub_title">Stage de deux mois<div class="bar"></div> 2019.</div>
            <div class="title">Axopen.</div>
        </div>
        <div link-work="abc" class="work_cont hover-target" data-smooth-from="-120px" data-smooth-to="120px" style="margin-top:2em;">    
            <div class="work_img_cont_parent"><div class="work_img_cont">
                <div class="work_bk" style="background:#942775;">
                    <div class="work_semi_circle">
                        <div class="semi_circle" style="transform:rotate(-90deg)">
                            <svg class="size1" width="250" height="120" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <circle cx="125" cy="125" r="125" fill="#BB004A" clip-path="url(#cut-off-bottom)" />
                            </svg>
                            <svg class="size2" width="180" height="90" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <circle cx="90" cy="90" r="90" fill="#BB004A" clip-path="url(#cut-off-bottom)" />
                            </svg>
                        </div>
                    </div>
                    <img class="img" style="width:60%" src="/assets/works/abc/imgs/logo.png">
                </div>
            </div></div>
            <div class="sub_title">Stage de 6 semaines<div class="bar"></div> 2018.</div>
            <div class="title">ABC Diffusion.</div>
        </div>
        <div link-work="jee" class="work_cont hover-target">    
            <div class="work_img_cont_parent"><div class="work_img_cont">
                <div class="work_bk" style="background:#3AA0FF;">
                    <div class="work_semi_circle">
                        <div class="semi_circle" style="transform:rotate(90deg)">
                            <svg class="size1" width="250" height="120" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <circle cx="125" cy="125" r="125" fill="#96CDFF" clip-path="url(#cut-off-bottom)" />
                            </svg>
                            <svg class="size2" width="180" height="90" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <circle cx="90" cy="90" r="90" fill="#96CDFF" clip-path="url(#cut-off-bottom)" />
                            </svg>
                        </div>
                    </div>
                    <img class="img" style="width:60%" src="/assets/works/jee/imgs/logo.png">
                </div>
            </div></div>
            <div class="sub_title">Travail scolaire<div class="bar"></div> 2018 / 2019.</div>
            <div class="title">Java Web.</div>
        </div>
    </div>
    <div class="works_bottom_page">
        <div class="got_project">Une question ?</div>
        <div id="works_contact_trigger_AP18F29" class="hover-target" text-hover="TCHAT">Me contacter.</div>       
        <div class="txt_btm_right_GQO92">
            <div class="hover-target" text-hover="DL"><a target="_blank" href="/assets/files/Franck-Desfrancais-CV.pdf">Curriculum vitae</a></div>
            <div>·</div>    
            <div>© 2019 Franck Desfrançais.</div>
        </div>      
    </div>
</div>
</body>

<script>
$(document).ready(function(){
    var scrollbar = Scrollbar.init(document.getElementById('works_page_21Y8J03'), { speed: 0.75 });

    // get all smooth enter effect
    var controller = new ScrollMagic.Controller();
    var wHeight = $(window).height();
    $("#works_page_21Y8J03").find("[data-smooth-from]").each(function(){
        var tween = TweenMax.fromTo(this, 1, {top: this.getAttribute('data-smooth-from')}, {top: this.getAttribute('data-smooth-to'),ease: Power0.easeNone});
        var scene = new ScrollMagic.Scene({triggerElement:this, duration:wHeight, offset:-wHeight/2})
        .setTween(tween).addTo(controller);
    });
    $("#works_page_21Y8J03").find(".work_img_cont").each(function(){
        var scene = new ScrollMagic.Scene({triggerElement:this, offset:-wHeight/6})
        .setClassToggle(this, "active").addTo(controller);
    });

    //clicks
    $("#works_contact_trigger_AP18F29").click(function(){
        transition1(global_current_page,global_contact_page);
    });
    $('.work_cont').click(function(){
        //check if link to external page or link-work
        var link = $(this).attr('link-page');
        if(link != null){
            openExternalPage(link);
        }
        
        link = $(this).attr('link-work');
        if(link == null){
            return;
        }

        transitionLoading({type:"in"});
        $("#page-work").load("/openwork/"+link, function() {
            link = "/work/"+link;
            var next_page = {div:"page-work",page_name:"work",url:link};
            transitionToWork(global_current_page,next_page);
            transitionLoading({type:"out"});
        });
    });
    $("#works_contact_trigger_AP18F29").click(function(){
        transition1(global_current_page,global_contact_page);
    });
    
    function openExternalPage(link){
        window.open("http://"+link);
        e.preventDefault(); //or return false;
    }
});
</script>