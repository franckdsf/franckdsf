$(document).ready(function(){
  setTimeout(() => {
      refreshHoverTargets();
      loadPage();
  }, 500);

  function loadPage(){
      var scrollbar = Scrollbar.init(document.getElementById('QJGIO190'), { speed: 0.75 });

      //load lower_res img
      $("[preload]").each(function(){
        var src = this.getAttribute('src');
        $(this).attr("src",this.getAttribute('preload'));
        
        const img = new Image();
        img.src = src;
        img.onload = () => {
          $(this).attr("src", src);
        };
      });

      //click
      $('#work_contact_me_button').click(function(){
          transition1(global_current_page, global_contact_page);
      });
      $('#next_project_trigger').click(function(){
          var link = $(this).attr('link-work');
          if(link == null)
              return;
          
          transition_LR({type:"in"});
          setTimeout(() => {
              $("#page-work").load("/openwork/"+link, function() {
                  link = "/work/"+link;
                  var next_page = {div:"page-work",page_name:"work",url:link};
                  setTimeout(() => {
                      transition_LR({type:"out",next:next_page});
                  }, 500);
              });
          }, 500);
      });


      // get all smooth enter effect
      var controller = new ScrollMagic.Controller();
      var wHeight = $(window).height();
      $("#QJGIO190").find("[data-smooth-from]").each(function(){
          $(this).css({"transition":"top 100ms"});
          var tween = TweenMax.fromTo(this, 1, {top: this.getAttribute('data-smooth-from')}, {top: this.getAttribute('data-smooth-to'),ease: Power0.easeNone});
          var scene = new ScrollMagic.Scene({triggerElement:this, duration:wHeight, offset:-wHeight/2})
          .setTween(tween).addTo(controller);
      });

      $("#QJGIO190").find("[data-pinned]").each(function(){

          $(this).css({'transition':'top linear 100ms'});

          var distance = this.parentElement.offsetHeight - this.offsetHeight;
          var tween = TweenMax.fromTo(this, 1, {top: 0}, {top: distance, ease: Power0.easeNone});
          var scene = new ScrollMagic.Scene({triggerElement:this.parentElement, duration:distance, offset:(wHeight-75)/2})
          .setTween(tween).addTo(controller);
      }); 
      $("#QJGIO190").find("[data-bk]").each(function(){
          var scene = new ScrollMagic.Scene({triggerElement:this})
          .setTween("#QJGIO190", 0.5, {background:$(this).attr('data-bk'), color:$(this).attr('data-color')}).addTo(controller);
      }); 


      //Bonus animations
      $("#next_project_trigger").mouseenter(function(){
          $(this).find(".arrow_next").css({left:0,opacity:0});
          $(this).find(".arrow_next").animate({
              left:"50%",
              opacity:1
          },{duration:500});
      });
      $("#next_project_trigger").mouseleave(function(){
          $(this).find(".arrow_next").animate({
              left:"100%",
              opacity:0
          },{duration:300});
      });
      $("#work_contact_me_button").mouseenter(function(){
          $(this).find(".bar").css({left:0,width:0});
          $(this).find(".bar").animate({
              left:0,
              width:"100%"
          },{duration:500});
      });
      $("#work_contact_me_button").mouseleave(function(){
          $(this).find(".bar").animate({
              left:"100%",
              width:"100%"
          },{duration:300});
      });
  }
});