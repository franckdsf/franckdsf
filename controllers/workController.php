<?php
  $work = $_GET['work'];
  $work = $_SERVER['DOCUMENT_ROOT'].'/views/works/'.$work.'.php';
  
  //Si le projet nexiste pas renvoyer sur index
  if (!file_exists($work)){
    header('Location: /');
    die;
  }

  //Si c'est un chargement depuis le projet
  //Charger l'application en entier
  //Sinon
  //Seulement charger la page cover

  $loadApp = false;
  if(isset($_GET["loadApp"])){
    $loadApp = true;
  }

  if($loadApp){
    require($_SERVER['DOCUMENT_ROOT']."/controllers/appController.php");  
  }
  else{
    require($work);
  }
?>